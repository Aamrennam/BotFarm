# BotFarm
This crappy bot is a War and Order automation implementation using Powershell, Tesseract OCR and AutoItX. The project is licensed under the GPL. 


## Requirements
The following software and libraries are required to run BotFarm
* [Nox Player](https://www.bignox.com/)
* Powershell (only tested on version 5.1)
* [A .Net wrapper for tesseract-ocr](https://github.com/charlesw/tesseract)
* [AutoIt](https://www.autoitscript.com/site/autoit/)

## Setup
The only prerequisite is to install [Nox Player](https://www.bignox.com/). Powershell v.5.1 comes with newer Windows 10 distributions, and the remaining requirements are added to the repository so no manual effort is required.

##### Nox Player 
* Only tested under 1080x1920 on Mobile Phone settings

##### Windows settings
* Only tested with resolution 3200x1800 with 250% scaling (...Ughhh - lazy!)