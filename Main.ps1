﻿[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') 
Add-Type –AssemblyName PresentationFramework

Import-Module "$PSScriptRoot\AutoItX"
$Title = 'Ze crap bot'
$ButtonType = [System.Windows.MessageBoxButton]::OKCancel

$Options = [System.Windows.MessageBox]::Show('Please start War and Order using NOX player with Advanced options: ' + 
    [System.Environment]::NewLine + 'Startup settings: Mobile phone' +
    [System.Environment]::NewLine + 'Resolution: 1080x1920', $Title, $ButtonType)

If ($Options -match 'OK') {
    $Ready = [System.Windows.MessageBox]::Show('Have you set your fourth quick select march preset to monster attack?', 
            $Title, $ButtonType)
    If ($Ready -match 'OK') {
        $Minimum = [Microsoft.VisualBasic.Interaction]::InputBox("Please enter minimum monster attack level:", $title)
        $Maximum = [Microsoft.VisualBasic.Interaction]::InputBox("Please enter maximum monster attack level:", $title)
        . "$PSScriptRoot\WaOPSEngine\Start-VIPMonsterAttack.ps1"
        Start-VIPMonsterAttack -UsePotion -MonsterLevelMin $Minimum -MonsterLevelMax $Maximum -QuickSelect 4
    }
}