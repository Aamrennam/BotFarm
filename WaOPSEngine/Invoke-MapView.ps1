﻿#Requires -Modules "AutoItX"

Function Invoke-MapView {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, ValueFromPipeline=$true, 
            HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer")
    )

    Begin {
        (Join-Path $PSScriptRoot -ChildPath .\Function\Invoke-Screenshot.ps1 -Resolve)        
        . $PSScriptRoot\Invoke-GameButton.ps1
        . $PSScriptRoot\Invoke-CastleView.ps1
    }

    Process {
        # Look for exit dialogue            
        Show-AU3WinActivate -WinHandle $WinHandle | Out-Null
        Invoke-CastleView
        Start-Sleep -Milliseconds 200
        Invoke-GameButton -Button ButtonEnterMap
        Start-Sleep -Seconds 10   
    }
}