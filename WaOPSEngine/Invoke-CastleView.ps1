﻿#Requires -Modules "AutoItX"

Function Invoke-CastleView {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, ValueFromPipeline=$true, 
            HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer")
    )

    Begin {
        (Join-Path $PSScriptRoot -ChildPath .\Function\Invoke-Screenshot.ps1 -Resolve)        
        . $PSScriptRoot\Function\Get-GameText.ps1
        . $PSScriptRoot\Get-GameCoordinates.ps1
        . $PSScriptRoot\Invoke-GameButton.ps1
    }

    Process {
        $IsCastleView = $false
        do {
            # Look for exit dialogue            
            Show-AU3WinActivate -WinHandle $WinHandle | Out-Null
            Send-AU3Key -Key "{ESC}"
            
            $IsExit = Get-GameText -Position (Get-GameCoordinates -Item TextExitGame)
            Write-Verbose "Invoke-CastleView.ps1: Button text: $IsExit"
            If ($IsExit -match "Exit") {
                Invoke-GameButton ButtonExitNo
                $IsCastleView = $true
                Write-Verbose "Goto-CastleView.ps1: Castle view = $IsCastleView"
                continue
            }
            Start-Sleep -Seconds 3
        } while (-not $IsCastleView) 
    }
}