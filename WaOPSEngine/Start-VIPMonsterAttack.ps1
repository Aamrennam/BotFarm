﻿Function Start-VIPMonsterAttack {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [Switch]$UsePotion,
        [Int]$Count,
        [Parameter(Mandatory=$true)]
        [Int]$MonsterLevelMin,
        [Parameter(Mandatory=$true)]
        [Int]$MonsterLevelMax,
        [Parameter(Mandatory=$true, HelpMessage="Please choose army quick select preset.")]
        [Int]$QuickSelect
    )

    Begin {
        # Dot sourcing scripts 
        . "$PSScriptRoot\Invoke-GameButton.ps1"
        . "$PSScriptRoot\Invoke-MapView.ps1"
        . "$PSScriptRoot\Get-VIPStatus.ps1"
        . "$PSScriptRoot\Find-VIPMonster.ps1"
        . "$PSScriptRoot\Confirm-MonsterAttack.ps1"
        . "$PSScriptRoot\Wait-Attack.ps1"
        . "$PSScriptRoot\Use-Potion.ps1"
        $Counter = 0
        If (-not $Count) {
            $Count = 9999
        }
    }

    Process {
        # Go to correct view
        Invoke-MapView

        If (Get-VIPStatus) {
            While ($Counter -lt $Count) {
                Wait-Attack -Verbose
                $MonsterLevel = Get-Random -Minimum $MonsterLevelMin -Maximum ($MonsterLevelMax + 1)
                Find-VIPMonster -MonsterLevel $MonsterLevel -Verbose
                Confirm-MonsterAttack -QuickSelect $QuickSelect -Verbose 
                If ($UsePotion) {
                    Use-Potion -Verbose
                }
                Wait-Attack -Verbose
                $Counter++
            }
        }
    }

    End {
    }
}