﻿#Requires -Modules "AutoItX"

Function Invoke-GameButton {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [Parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0,
                   HelpMessage="Type of dialoguebox to check get coords for.")]
        [Alias("Item")]        
        [String]$Button
        
    )
    
    Begin {
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
        . "$PSScriptRoot\function\Get-GameText.ps1"
        $TempFolder = Join-Path $PSScriptRoot -ChildPath ..\temp -Resolve
    }

    Process {
        $Position = Get-GameCoordinates -Item $Button 
        $Click = Invoke-AU3MouseClick -Button Left -X $Position.X -Y $Position.Y -Speed 500 
        Start-Sleep -Milliseconds 200
    }
}
