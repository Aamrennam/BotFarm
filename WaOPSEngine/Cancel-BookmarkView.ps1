﻿#Requires -Modules "AutoItX"

Function Cancel-BookmarkView {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer")
    )

    Begin {
        . "$PSScriptRoot\Invoke-GameButton.ps1"
        . "$PSScriptRoot\Function\Get-GameText.ps1"
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
    }

    Process {
        $Text = Get-GameText -Position (Get-GameCoordinates -Item TextBookmarkTitle)
        if ($Text -match "Bookmark") {
            Write-Verbose "Cancel-Bookmark.ps1: Closing bookmark."
            Invoke-GameButton -Button CloseBookmark
            return $true
        } else {
            Write-Verbose "Cancel-Bookmark.ps1: Bookmark dialogue not open."
            return $false
        }
    }
}