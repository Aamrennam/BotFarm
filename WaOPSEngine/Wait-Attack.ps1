﻿Function Wait-Attack {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, ValueFromPipeline=$true, 
            HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer")
    )

    Begin {
        . "$PSScriptRoot\function\Get-GameText.ps1"
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
    }

    Process {
        
        $MarchTime = Get-GameText -Position (Get-GameCoordinates TextMarchTime -Verbose) -Grayscale
        Write-Verbose "Wait-Attack.ps1: March time checked. $MarchTime."         
        If (($MarchTime -match "EnRoute") -or ($MarchTime -match "Withdraw")) {
            $regex = $MarchTime -match "([0-9][0-9]):([0-9][0-9])"
            [int]$Min = [int]$Matches[1] * 60
            [int]$Seconds = [int]$Matches[2]
            $SleepTime = $Min + $Seconds + 3
            Write-Verbose "Wait-Attack.ps1: Sleeping $SleepTime seconds."
            Start-Sleep -Seconds $SleepTime -Verbose
            Continue
        }

        if ($MarchTime -match "EnRoute") {
            Wait-Attack
        }
    }
}