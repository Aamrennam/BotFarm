﻿#Requires -Modules "AutoItX"

Function Get-GameCoordinates {
<#
.SYNOPSIS
    This function returns a [System.Drawing.Rectangle] as position to 
.
#>
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [Parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0)] 
        [String]$Item
    )

    Process {
        $Position = Get-AU3WinPos -WinHandle $WinHandle
        $Coordinates = Switch ($Item) {
            {($_ -match "ButtonConfirmBookmark") -or ($_ -match "ButtonTextAttack")} {            
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.44, # 0.294 `
                    $Position.Y + $Position.Height *  0.665, `
                    $Position.X + $Position.Width * 0.56, `
                    $Position.Y + $Position.Height * 0.695)         
            } 
            
            "ButtonCloseBookmark" {
                [Drawing.Rectangle]::FromLTRB(
                    $Position.X + $Position.Width * 0.87, `
                    $Position.Y + $Position.Height * 0.31, `
                    $Position.X + $Position.Width * 0.87, `
                    $Position.Y + $Position.Height * 0.31) 

            }

            "TextBookmarkTitle" {            
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.218, `
                    $Position.Y + $Position.Height * 0.2915, `
                    $Position.X + $Position.Width * 0.782, `
                    $Position.Y + $Position.Height * 0.339) 
            } 
            
            "TextStamina" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.2075, `
                    $Position.Y + $Position.Height * 0.036, `
                    $Position.X + $Position.Width * 0.251, `
                    $Position.Y + $Position.Width * 0.12)
            } 
            
            "TextMarchTime" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.065, `
                    $Position.Y + $Position.Height * 0.1945, `
                    $Position.X + $Position.Width * 0.251, `
                    $Position.Y + $Position.Height * 0.228)
            } 

            "TextVIPStatus" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.5, `
                    $Position.Y + $Position.Height * 0.132, `
                    $Position.X + $Position.Width * 0.7, `
                    $Position.Y + $Position.Height * 0.15)
            } 

            "TextExitGame" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.38, `
                    $Position.Y + $Position.Height * 0.46, `
                    $Position.X + $Position.Width * 0.63, `
                    $Position.Y + $Position.Height * 0.49)
            }

            "ButtonExitNo" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.67, `
                    $Position.Y + $Position.Height * 0.56, `
                    $Position.X + $Position.Width * 0.67, `
                    $Position.Y + $Position.Height * 0.56)
            }

            "ButtonExitYes" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.32, `
                    $Position.Y + $Position.Height * 0.56, `
                    $Position.X + $Position.Width * 0.32, `
                    $Position.Y + $Position.Height * 0.56)
            }

            {($_ -match "ButtonEnterMap") -or ($_ -match "ButtonEnterCastle")} {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.115, `
                    $Position.Y + $Position.Height * 0.95, `
                    $Position.X + $Position.Width * 0.115, `
                    $Position.Y + $Position.Height * 0.95)
            }

            "ButtonVIP" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.55, `
                    $Position.Y + $Position.Height * 0.08, `
                    $Position.X + $Position.Width * 0.55, `
                    $Position.Y + $Position.Height * 0.08)
            }

            "ButtonVIPSearch" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.93, `
                    $Position.Y + $Position.Height * 0.79, `
                    $Position.X + $Position.Width * 0.93, `
                    $Position.Y + $Position.Height * 0.79)
            }

            "ButtonVIPSearchMonster" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.89, `
                    $Position.Y + $Position.Height * 0.82, `
                    $Position.X + $Position.Width * 0.89, `
                    $Position.Y + $Position.Height * 0.82)
            }

            "TextboxVIPLevel" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.82, `
                    $Position.Y + $Position.Height * 0.895, `
                    $Position.X + $Position.Width * 0.82, `
                    $Position.Y + $Position.Height * 0.895)
            }

            "ButtonOK" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.95, `
                    $Position.Y + $Position.Height * 0.97, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonGO" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.83, `
                    $Position.Y + $Position.Height * 0.95, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonBack" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.075, `
                    $Position.Y + $Position.Height * 0.06, `
                    $Position.X + $Position.Width * 0.075, `
                    $Position.Y + $Position.Height * 0.06)
            }

            "DefaultMonsterPosition" { 
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.5, `
                    $Position.Y + $Position.Height * 0.4, `
                    $Position.X + $Position.Width * 0.5, `
                    $Position.Y + $Position.Height * 0.4)
            }

            "ButtonQuickSelect1" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.525, `
                    $Position.Y + $Position.Height * 0.21, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonQuickSelect2" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.625, `
                    $Position.Y + $Position.Height * 0.21, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }


            "ButtonQuickSelect3" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.725, `
                    $Position.Y + $Position.Height * 0.21, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonQuickSelect4" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.825, `
                    $Position.Y + $Position.Height * 0.21, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonQuickSelect5" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.925, `
                    $Position.Y + $Position.Height * 0.21, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonSetOut" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.83, `
                    $Position.Y + $Position.Height * 0.955, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "TextUseStamina" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.4, `
                    $Position.Y + $Position.Height * 0.236, `
                    $Position.X + $Position.Width * 0.6, `
                    $Position.Y + $Position.Height * 0.257)
            }

            "ButtonUse20Stamina" {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.785, `
                    $Position.Y + $Position.Height * 0.418, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }

            "ButtonUseConfirm" { 
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.X + $Position.Width * 0.5, `
                    $Position.Y + $Position.Height * 0.55, `
                    $Position.X + $Position.Width * 0, `
                    $Position.Y + $Position.Height * 0)
            }
            
            default {
                [Drawing.Rectangle]::FromLTRB( `
                    $Position.Left, `
                    $Position.Top, `
                    $Position.Right, `
                    $Position.Bottom)
            }
        }

        Write-Verbose "Get-GameCoordinate.ps1: $Item - $Coordinates"
        Return $Coordinates
    }
}

### For testing purposes...
#Buttons
#$c = Get-GameCoordinates -Item ButtonUseConfirm
#Invoke-AU3MouseClick -Button Right -X $C.X -Y $C.Y

#Textblocks
#$c = Get-GameCoordinates -Item TextUseStamina
#Invoke-AU3MouseClick -Button Right -X $c.Left -Y $c.Top
#Start-Sleep -Milliseconds 100
#Invoke-AU3MouseClick -Button Right -X $c.Left -Y $c.Bottom
#Start-Sleep -Milliseconds 100
#Invoke-AU3MouseClick -Button Right -X $c.Right -Y $c.Bottom
#Start-Sleep -Milliseconds 100
#Invoke-AU3MouseClick -Button Right -X $c.Right -Y $c.Top