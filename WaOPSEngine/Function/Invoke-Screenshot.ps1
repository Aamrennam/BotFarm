﻿#Requires -Modules "AutoItX"

Function Invoke-Screenshot {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$false, ValueFromPipeline=$true, 
            HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [Parameter(Mandatory=$true, HelpMessage="Path to save screenshot")]
        [String]$Path,
        [System.Drawing.Rectangle]$Position
    )

    Begin {
        [Reflection.Assembly]::LoadWithPartialName("System.Drawing") | Out-Null
    }

    Process {        
        Write-Verbose "Invoke-Screenshot.ps1: [System.Drawing.Rectangle] - $Position"
        $Show = Show-AU3WinActivate -WinHandle $WinHandle              
        $Source = New-Object Drawing.Bitmap $Position.Width, $Position.Height
        $Graphics = [Drawing.Graphics]::FromImage($Source)    
        $Graphics.CopyFromScreen($Position.Location, [System.Drawing.Point]::Empty, $Position.Size, [System.Drawing.CopyPixelOperation]::SourceCopy)  
        $Source.Save($Path, [System.Drawing.Imaging.ImageFormat]::Png)
        $Path        
    }

    End {
        $Graphics.Dispose()
        $Source.Dispose()
    }
}