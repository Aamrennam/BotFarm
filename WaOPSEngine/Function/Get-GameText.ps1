﻿Function Get-GameText {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, ValueFromPipeline=$true, 
            HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [System.Drawing.Rectangle]$Position, 
        [Switch]$hOCR,
        [Switch]$Grayscale
    )

    Begin { 
        $TempFolder = (Join-Path -Path $PSScriptRoot -ChildPath ..\..\temp -Resolve)
        . "$PSScriptRoot\Invoke-Screenshot.ps1"
        . "$PSScriptRoot\Extract-TesseractText.ps1"
        $ImageFile = [System.Guid]::NewGuid()
    }

    Process {        
        $Path = Invoke-Screenshot -Path $TempFolder\$ImageFile -Position $Position -Verbose
        Write-Verbose "Get-GameText.ps1: Temporary image path - $Path"
        
        return Extract-TesseractText -Path $Path -hOCR:$hOCR -Grayscale:$Grayscale -Verbose    
    }

    End {
        Start-Sleep -Milliseconds 100
        # Comment out line below to save image files.
        Remove-Item $TempFolder\$ImageFile | Out-Null
    }
}

#### For testing purposes
#$waofarmengine = (Join-Path $PSScriptRoot -ChildPath ..\)
#. "$waofarmengine\Get-GameCoordinates.ps1"
#$t = Get-GameText -Position (Get-GameCoordinates -Item ButtonTextAttack) -Grayscale