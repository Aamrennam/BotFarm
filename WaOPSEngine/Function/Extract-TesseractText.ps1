﻿Function Extract-TesseractText {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$true, HelpMessage="Image path to extract text from")]
        [ValidateScript({ Test-Path $_ })]
        [Alias("ImagePath", "Image")]
        [String]$Path,
        [Switch]$hOCR,
        [Switch]$Grayscale
    )

    Begin {        
        $LibFolder = Join-Path -Path $PSScriptRoot -ChildPath ..\..\lib -Resolve
        $TempFolder = Join-Path -Path $PSScriptRoot -ChildPath ..\..\temp -Resolve
        . "$PSScriptRoot\Convert-ImageToGrayscale.ps1"
        #Import System.Drawing and Tesseract libraries
        Add-Type -AssemblyName "System.Drawing"
        Add-Type -Path "$LibFolder\Tesseract.dll"
        #Create tesseract object, specify tessdata location and language
        $tesseract = New-Object Tesseract.TesseractEngine((Get-Item "$LibFolder\tessdata").FullName, "eng", [Tesseract.EngineMode]::Default, $null) 
    }

    Process {
        # Verifying image path
        $Path = Resolve-Path $Path
        Write-Verbose "Extract-TesseractText.ps1: Image path: $Path"
        Test-Path $Path -ErrorAction Stop | Out-Null

        If ($Grayscale) {
            # Convert image to Grayscale
            $GrayscaleDestination = "$TempFolder\$([System.Guid]::NewGuid())"
            Write-Verbose "Extract-TesseractText.ps1: Converting image to grayscale image (destination: $GrayscaleDestination)"
            $Path = Convert-ImageToGrayScale $Path -Destination $GrayscaleDestination
        }

        # Load and process image
        $Image = New-Object System.Drawing.Bitmap($Path)
        $Pix = [Tesseract.PixConverter]::ToPix($Image)
        $Page = $Tesseract.Process($Pix)
        # Extract and return text from image
        If ($hOCR) {
            $Result = $Page.GetHOCRText(0, $false)
            Write-Verbose "Extract-TesseractText.ps1: Result type [$($Result.GetType())]."
        } else {
            $Result = $Page.GetText()
            $Result = $Result -replace "\s+", ""
            Write-Verbose "Extract-TesseractText.ps1: Result = '$Result'"
        }        
        return $result
    } 
    
    End {
        #Cleanup
        $image.Dispose()
        $page.Dispose()
        If ($Grayscale) {
            Remove-Item $GrayscaleDestination
        }
    }
}

