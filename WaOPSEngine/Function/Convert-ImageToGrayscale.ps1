﻿
Function Convert-ImageToGrayScale { 
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true, Position=0, ValueFromPipeline = $true,
                    HelpMessage = "The path to the image to convert to grayscale.")]
        [Alias("ImagePath", "Image", "FilePath")]
        [String]$Path,
        [Parameter(Position = 1, HelpMessage = "Destination for grayscale image.")]
        [String]$Destination
    ) 
    
    Begin {
        [void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
        [void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing.Imaging") 
        [void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

        # Set values to convert image to grayscale
        $Matrix = New-Object System.Drawing.Imaging.Colormatrix         
        $Matrix.Matrix00 = 0.3 
        $Matrix.Matrix01 = 0.3 
        $Matrix.Matrix02 = 0.3 
        $Matrix.Matrix03 = 0.0 
        $Matrix.Matrix04 = 0.0          
        $Matrix.Matrix10 = 0.59 
        $Matrix.Matrix11 = 0.59 
        $Matrix.Matrix12 = 0.59 
        $Matrix.Matrix13 = 0.0 
        $Matrix.Matrix14 = 0.0          
        $Matrix.Matrix20 = 0.11 
        $Matrix.Matrix21 = 0.11 
        $Matrix.Matrix22 = 0.11 
        $Matrix.Matrix23 = 0.0 
        $Matrix.Matrix24 = 0.0          
        $Matrix.Matrix30 = 0.0 
        $Matrix.Matrix31 = 0.0 
        $Matrix.Matrix32 = 0.0 
        $Matrix.Matrix33 = 1.0 
        $Matrix.Matrix34 = 0.0          
        $Matrix.Matrix40 = 0.0 
        $Matrix.Matrix41 = 0.0 
        $Matrix.Matrix42 = 0.0 
        $Matrix.Matrix43 = 0.0 
        $Matrix.Matrix44 = 1.0          
        $Attributes = New-Object System.Drawing.Imaging.ImageAttributes 
    }     

    Process {
        Write-Verbose "Convert-ImageToGrayscale.ps1: Image path $Path"
        $Original = New-Object System.Drawing.Bitmap($Path)
        $Grayscale = New-Object System.Drawing.Bitmap($Original.Width, $Original.Height)
        $Graphics = [System.Drawing.Graphics]::FromImage($Grayscale)
        $Attributes.SetColorMatrix($Matrix)          
        $imageRectangle = New-Object System.Drawing.Rectangle(0, 0, $Original.Width, $Original.Height) 
        $Graphics.DrawImage($Original, $ImageRectangle, 0, 0, $Original.Width, $Original.Height, [System.Drawing.GraphicsUnit]::Pixel, $Attributes) 
        If ($Destination) {
            Write-Verbose "Convert-ImageToGrayscale.ps1: Saving grayscale image to $Destination"
            $Grayscale.Save($Destination, [System.Drawing.Imaging.ImageFormat]::Png)
            return $Destination
        } Else {
            Write-Verbose "Convert-ImageToGrayscale.ps1: $Grayscale"
            return $Grayscale
        }
    }
    
    End {
        $Grayscale.Dispose()
        $Graphics.Dispose() 
        $Original.Dispose()
    }
} 