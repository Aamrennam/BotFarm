﻿#Requires -Modules "AutoItX"

Function Get-VIPStatus {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer")
    )

    Begin {
        . "$PSScriptRoot\Invoke-GameButton.ps1"
        . "$PSScriptRoot\Function\Get-GameText.ps1"
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
    }

    Process {
        Invoke-GameButton ButtonVIP
        $ImageText = Get-GameText -Position (Get-GameCoordinates -Item TextVIPStatus)
        If ($ImageText -match "Not") {
            Write-Verbose "Get-VIPStatus.ps1: VIP not active!"
            Invoke-GameButton ButtonBack
            Return $False
        } else {
            Write-Verbose "Get-VIPStatus.ps1: VIP active time remaining: $ImageText!"
            Invoke-GameButton ButtonBack
            Return $True
        }
    }
}