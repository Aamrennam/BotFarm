﻿#Requires -Modules "AutoItX"

Function Find-VIPMonster {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [Int]$MonsterLevel
    )

    Begin {
        . "$PSScriptRoot\Invoke-GameButton.ps1"
        . "$PSScriptRoot\Function\Get-GameText.ps1"
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
    }

    Process {
        Show-AU3WinActivate -WinHandle $WinHandle | Out-Null
        Invoke-GameButton -Button ButtonVIPSearch
        Invoke-GameButton -Button ButtonVIPSearchMonster
        Invoke-GameButton -Button TextboxVIPLevel
        Start-Sleep -Milliseconds 500
        Send-AU3Key -Key "$MonsterLevel"
        Invoke-GameButton -Button ButtonOK
        Invoke-GameButton -Button ButtonGO
    }
}