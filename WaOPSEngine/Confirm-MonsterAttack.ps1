﻿Function Confirm-MonsterAttack {
    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer"),
        [System.Drawing.Rectangle]$Position,
        [Parameter(Mandatory=$false, HelpMessage="Amount of times to retry monster before resetting map view.")]
        [Int]$Retry = 7,
        [Int]$QuickSelect = 1
    )

    Begin {
        . "$PSScriptRoot\Function\Get-GameText.ps1"
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
        . "$PSScriptRoot\Cancel-BookmarkView.ps1"
        . "$PSScriptRoot\Invoke-MapView"
        $OriginalRetry = $Retry
    }

    Process {
        If (-not $Position) { 
            $Position = Get-GameCoordinates -Item DefaultMonsterPosition -Verbose
        } 
       
        $NotAttacking = $true
        While ($NotAttacking) {
            Invoke-AU3MouseClick -Button Left -X $Position.X -Y $Position.Y -Speed 500
        
            # Check for attack confirmation
            $TextAttack = Get-GameText -Position (Get-GameCoordinates -Item ButtonTextAttack) -Grayscale -Verbose     
            If ($TextAttack -match "Attack") {
                Write-Verbose "Confirm-MonsterAttack.ps1: Set army and attack!"
                Invoke-GameButton ButtonTextAttack -Verbose
                Invoke-GameButton "ButtonQuickSelect$QuickSelect" -Verbose
                Invoke-GameButton ButtonSetOut -Verbose
                $NotAttacking = $false
                continue                
            }

            $Position.X = $Position.X + (Get-Random -Minimum -50 -Maximum 50)
            $Position.Y = $Position.Y + (Get-Random -Minimum -60 -Maximum 20)
            Cancel-BookmarkView -Verbose
            Start-Sleep -Seconds 1
            If ($Retry -le 0) {
                Invoke-MapView
                $NotAttacking = $false
            } else {
                $Retry -= 1
            }
        }
    }
}