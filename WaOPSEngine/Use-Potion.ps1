﻿#Requires -Modules "AutoItX"

Function Use-Potion {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false, HelpMessage="Please use valid emulator window handle.")]
        [Alias("Handle", "hWnd")]
        [System.IntPtr]$WinHandle = (Get-AU3WinHandle -Title "NoxPlayer")
    )
    
    Begin {
        . "$PSScriptRoot\Function\Get-GameText.ps1"
        . "$PSScriptRoot\Get-GameCoordinates.ps1"
        . "$PSScriptRoot\Invoke-GameButton.ps1"
    }

    Process {
        $MoreStamina = Get-GameText -Position (Get-GameCoordinates -Item TextUseStamina) -Verbose
        If ($MoreStamina -match "MoreStamina") {
            Invoke-GameButton -Button ButtonUse20Stamina
            Invoke-GameButton -Button ButtonUseConfirm
            Invoke-GameButton -Button ButtonBack
            Invoke-GameButton -Button ButtonSetOut
            return $true
        }
    }
}

Use-Potion -Verbose